resource "aws_ecr_repository" "ecr_repo_s3_uploader" {
  name = local.ECR_REPO_S3_UPLOADER_NAME
  encryption_configuration {
    encryption_type = local.ECR_REPO_ENCRYPTION_TYPE
  }
  image_tag_mutability = local.ECR_REPO_IMG_TAG_MUTABILITY
  image_scanning_configuration {
    scan_on_push = local.ECR_REPO_SCAN_ON_PUSH
  }
  tags = {
    Name = local.ECR_REPO_S3_UPLOADER_NAME
  }
  # lifecycle {
  #   prevent_destroy = true
  # }
}
