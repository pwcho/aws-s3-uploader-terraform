resource "aws_lambda_function" "lambda_s3_uploader" {
  function_name = local.LAMBDA_S3_UPLOADER_NAME
  role          = aws_iam_role.iam_role_lambda_s3_uploader.arn
  architectures = ["x86_64"]
  ephemeral_storage {
    size = 512
  }
  timeout      = local.LAMBDA_S3_UPLOADER_TIMEOUT
  memory_size  = local.LAMBDA_S3_UPLOADER_MEM
  package_type = "Image"
  tracing_config {
    mode = "PassThrough"
  }
  environment {
    variables = {
      SECRET_KEY    = local.SECRET_KEY
      COMMAND       = "app.lambda.handler",
      SETUP_ENV     = "lambda",
      AWS_S3_REGION = "eu-central-1",
      AWS_S3_BUCKET = "bhsdvuhnwejcu-s3-uploader-bucket"
    }
  }
  image_uri = "${aws_ecr_repository.ecr_repo_s3_uploader.repository_url}:${local.LAMBDA_S3_UPLOADER_VER}"
  tags = {
    Name = local.LAMBDA_S3_UPLOADER_NAME
  }
  depends_on = [
    aws_ecr_repository.ecr_repo_s3_uploader,
    aws_s3_bucket.s3_uploader_bucket
  ]
}
