resource "aws_s3_bucket" "s3_uploader_bucket" {
  bucket = local.S3_UPLOADER_BUCKET
}

resource "aws_s3_bucket_accelerate_configuration" "s3_bucket_acl_conf" {
  bucket = aws_s3_bucket.s3_uploader_bucket.bucket
  status = "Enabled"
}

resource "aws_s3_bucket_public_access_block" "s3_bucket_pab" {
  bucket                  = aws_s3_bucket.s3_uploader_bucket.bucket
  block_public_acls       = false
  block_public_policy     = false
  restrict_public_buckets = false
  ignore_public_acls      = false
}

resource "aws_s3_bucket_lifecycle_configuration" "s3_bucket_lifecycle_config" {
  bucket = aws_s3_bucket.s3_uploader_bucket.bucket

  rule {
    id = "s3_bucket_lifecycle_config"

    expiration {
      days = local.S3_UPLOAD_DAYS_TTL
    }

    filter {}

    status = "Enabled"

    # transition {
    #   days          = 30
    #   storage_class = "STANDARD_IA"
    # }

    # transition {
    #   days          = 60
    #   storage_class = "GLACIER"
    # }
  }

  #   rule {
  #     id = "tmp"

  #     filter {
  #       prefix = "tmp/"
  #     }

  #     expiration {
  #       date = "2023-01-13T00:00:00Z"
  #     }

  #     status = "Enabled"
  #   }
}
