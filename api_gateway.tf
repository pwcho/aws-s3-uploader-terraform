# resource "aws_apigatewayv2_api" "http_api_gateway" {
#   name          = "http_api_gateway"
#   protocol_type = "HTTP"
#   cors_configuration {
#     allow_credentials = false
#     allow_headers     = ["authorization", "*"]
#     allow_methods     = ["GET", "POST", "PUT", "DELETE"]
#     allow_origins     = ["*"]
#   }
# }

# resource "aws_apigatewayv2_stage" "http_api_gateway_default_stage" {
#   api_id      = aws_apigatewayv2_api.http_api_gateway.id
#   name        = "$default"
#   auto_deploy = true
# }

# # resource "aws_apigatewayv2_integration" "http_api_gateway_lambda_s3_uploader_integration" {
# #   api_id                 = aws_apigatewayv2_api.http_api_gateway.id
# #   integration_type       = "AWS_PROXY"
# #   integration_method     = "POST"
# #   connection_type        = "INTERNET"
# #   payload_format_version = "2.0"
# #   integration_uri        = aws_lambda_function.lambda_s3_uploader.invoke_arn
# #   depends_on = [
# #     aws_apigatewayv2_api.http_api_gateway
# #   ]
# # }

# resource "aws_apigatewayv2_route" "http_api_gateway_s3_uploader_route" {
#   api_id             = aws_apigatewayv2_api.http_api_gateway.id
#   api_key_required   = false
#   authorization_type = "NONE"
#   route_key          = "POST /api/v1/s3/file"
#   target             = "integrations/${aws_apigatewayv2_integration.http_api_gateway_lambda_s3_uploader_integration.id}"
#   depends_on = [
#     aws_apigatewayv2_api.http_api_gateway,
#     aws_apigatewayv2_integration.http_api_gateway_lambda_s3_uploader_integration,
#   ]
# }

# resource "aws_lambda_permission" "lambda_permission_s3_uploader" {
#   statement_id  = "AllowAPIGatewayInvoke"
#   action        = "lambda:InvokeFunction"
#   function_name = aws_lambda_function.lambda_s3_uploader.function_name
#   principal     = "apigateway.amazonaws.com"
#   source_arn    = "${aws_apigatewayv2_api.http_api_gateway.execution_arn}/*/*"
# }
