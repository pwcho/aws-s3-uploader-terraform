data "aws_iam_policy_document" "iam_policy_document_lambda_invoke_access" {
  statement {
    sid    = "TriggerInvokeLambda"
    effect = "Allow"
    actions = [
      "lambda:InvokeAsync",
      "lambda:InvokeFunction"
    ]
    resources = ["*"]
  }
}

data "aws_iam_policy_document" "iam_policy_document_log_access" {
  statement {
    sid    = "WriteLogStreamsAndGroups"
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = ["*"]
  }
  statement {
    sid       = "CreateLogGroup"
    effect    = "Allow"
    actions   = ["logs:CreateLogGroup"]
    resources = ["*"]
  }
}

data "aws_iam_policy_document" "iam_policy_document_s3_bucket_work_kmd_access" {
  statement {
    sid = ""

    actions = [
      "s3:*"
    ]

    resources = [
      "arn:aws:s3:::*"
    ]
  }
}

resource "aws_iam_policy" "iam_policy_lambda_invoke_access" {
  name   = "iam_policy_lambda_invoke_acces"
  policy = data.aws_iam_policy_document.iam_policy_document_lambda_invoke_access.json
}

resource "aws_iam_policy" "iam_policy_log_access" {
  name   = "iam_policy_log_access"
  policy = data.aws_iam_policy_document.iam_policy_document_log_access.json
}

resource "aws_iam_policy" "iam_policy_s3_bucket_access" {
  name   = "iam_policy_s3_bucket_work_kmd_access"
  policy = data.aws_iam_policy_document.iam_policy_document_s3_bucket_work_kmd_access.json
}

resource "aws_iam_role" "iam_role_lambda_s3_uploader" {
  name                  = "iam_role_lambda_s3_uploader"
  force_detach_policies = true
  assume_role_policy    = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "iam_role_policy_attachment_lambda_s3_uploader_invoke_access" {
  role       = aws_iam_role.iam_role_lambda_s3_uploader.name
  policy_arn = aws_iam_policy.iam_policy_lambda_invoke_access.arn
}

resource "aws_iam_role_policy_attachment" "iam_role_policy_attachment_lambda_s3_uploader_log_access" {
  role       = aws_iam_role.iam_role_lambda_s3_uploader.name
  policy_arn = aws_iam_policy.iam_policy_log_access.arn
}

resource "aws_iam_role_policy_attachment" "iam_role_policy_attachment_lambda_s3_uploader_s3_access" {
  role       = aws_iam_role.iam_role_lambda_s3_uploader.name
  policy_arn = aws_iam_policy.iam_policy_s3_bucket_access.arn
}
